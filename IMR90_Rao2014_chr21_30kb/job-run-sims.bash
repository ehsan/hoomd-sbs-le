#!/bin/bash

#$ -l data
#$ -l h_gpu=1
#$ -cwd
#$ -V
#$ -m be
#$ -l m_mem_free=10G
#$ -l h_rt=24:00:00
#$ -now n

GUIX_PROFILE="/gnu/var/guix/profiles/per-user/eirani/guix-profile"
. "$GUIX_PROFILE/etc/profile"

for id in $(seq $1 $2); do singularity exec -B /data:/data --nv ~/softwares/ehsan-singularity-hoomdv2.8.1.simg python3 ./sbs-le-classic.py --settings IMR90_Rao2014_chr21_30kb.settings_sbs_le_IMR90_Rao2014_chr21_30kb.py --sim-id sbs-$id --hoomd 'gpu'; done

