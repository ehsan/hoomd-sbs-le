import numpy as np, inspect, pandas as pd
import hoomd, ehsan_loopextrusion



##############################
## Simulation parameters
##############################
thermal_timesteps = 3e5
final_timesteps = 5e6
dump_period = 1e3
restart_period = 5e6
dump_fname = lambda sim_id : r'gsds-LEtex5e4/{}-dump.gsd'.format(sim_id)
restart_fname = lambda sim_id : r'gsds-LEtex5e4/{}-restart.gsd'.format(sim_id)

## MD parameters
dt = 0.01
KT = 1

## Simulation box
N = 0 # do not change this number
N_pol = 500
L0 = 2*N_pol**0.5887
L = np.array([L0,L0,L0])
Lhalf = 0.5*L
Dhalf = Lhalf
box = hoomd.data.boxdim(*L)

## LJ potential
sigma = 1.0
epsilon = 3.1
fene_r0 = 1.6
lj_rcut_rep = sigma*2**(1./6.) # act on all particles from different types
lj_rcut_att = sigma*2.5 # act on all particles and their corresponding binders
# average backgorund lj potential
epsilon_background = 8
lj_background_rcut = 1.3

## loop-extrusion
LE_bond_type = 'harmonic' # set to either 'harmonic' or 'fene'.
N_le = 10 # Number of loop extrusion agents/factors
le_period = 5e3 # Number of MD steps to update/slide the loop extrusion factor
le_k_off = 0.0125 # 1/80 kex/koff
le_extr_dist_thr = -1 # do not change it :)
le_type = 'loop_extrusion'
le_K = 10
le_r0 = 1.2
stop_probs = np.genfromtxt('CTCF.bed')[:,1:]
# option 2: Choose CTCF binding sites randomely from the file
# I will add it.

##############################
## particle, bond and angle types
## read the particle information from a file
##############################
binder_bead_ratio = 0.3
typeid_fname = 'polymer.bed'
polymer_df = pd.read_csv(typeid_fname, delim_whitespace=True, header=None, names=['pid', 'type'])
polymer_particle_types = list(set(polymer_df.type.values))
binder_particle_types = ['%s_binder'%x for x in polymer_particle_types]
particle_types = polymer_particle_types + binder_particle_types

bond_types = ['polymer', 'loop_extrusion']
le_typeid = bond_types.index(le_type)

snap_args = {'N':0 , 'box': box, 'particle_types': particle_types, 'bond_types': bond_types}

## define polymers 
polymers = []
pol_size = len(polymer_df)
for i in range(1):
    particles_typeid = np.array([particle_types.index(x) for x in polymer_df.type.values])
    #print ('particles_typeid : ', pol_size, particles_typeid)
    pol0 = dict(size=pol_size, bonds_typeid=0, particles_typeid=particles_typeid, pbc=np.array([True, True, True]), rcm_pos=np.zeros(3))
    polymers.append(pol0)

## define binders
binder_sizes = [int(binder_bead_ratio*np.sum(particles_typeid==particle_types.index(x))) for x in polymer_particle_types]
#print('binder_sizes', binder_sizes)
binders = [dict(size=binder_sizes[binder_particle_types.index(bt)], particles_typeid=particle_types.index(bt)) for bt in binder_particle_types]
print(binders)

